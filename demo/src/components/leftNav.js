import { Outlet,useNavigate  } from 'react-router-dom'
import './style.css'
const LeftNav = () => {
    const navigation=useNavigate()

    const goToHome=()=>{
        navigation('/home')
    }
    const goToHot=()=>{
        navigation('/hot')
    }
    const goToWrite=()=>{
        navigation('/write')
    }
    const goToPerson=()=>{
        navigation('/person')
    }
    return (
        <div className='wrap'>
            <div className='leftNav'>
                <button className='btn' onClick={()=>goToHome()}>Home</button>
                <button className='btn' onClick={()=>goToHot()}>Hot</button>
                <button className='btn' onClick={()=>goToWrite()}>Write</button>
                <button className='btn' onClick={()=>goToPerson()}>Person</button>
            </div>
            <Outlet></Outlet>
        </div>

    )
}

export default LeftNav