import { useNavigate } from 'react-router-dom'
import { connect } from 'react-redux';
import { clickAction} from './../store/index'

// 导入图片
import countImg from './../imgs/count.png'
import goodImg from './../imgs/good.png'
import badImg from './../imgs/bad.png'

const List = (props) => {
    let { dataArr ,updataClick} = props
    const navigation = useNavigate()

    const goToDetail = (item) => {
        navigation(`/page/${item.classify}/${item._id}`)
        item.count++
        updataClick(item.count,item._id,"count")
        for (let i = 0; i < dataArr.length; i++) {
            if (item._id == dataArr[i]._id) {
                let num = document.getElementsByClassName('countNum')[i]
                num.innerText++
            }
        }

    }
    const goodClick=(item,type)=>{
        if(type=="good"){
            item.good++
            updataClick(item.good,item._id,"good")

            for (let i = 0; i < dataArr.length; i++) {
                if (item._id == dataArr[i]._id) {
                    let num = document.getElementsByClassName('goodNum')[i]
                    num.innerText++
                }
            }
        }else{
            item.bad++
            updataClick(item.bad,item._id,"bad")
            for (let i = 0; i < dataArr.length; i++) {
                if (item._id == dataArr[i]._id) {
                    let num = document.getElementsByClassName('badNum')[i]
                    num.innerText++
                }
            }
        }
    }
    return (
        <div className="listWrap">
            <ul>
                {dataArr.map((item, index) => {
                    return (
                        <li key={index} className="pageItem">
                            <h2 className="pageTitle" onClick={() => goToDetail(item)}>{item.title}</h2>
                            <p className="pageContet" onClick={() => goToDetail(item)}>{item.content}</p>

                            <div className="pageFoot">
                                <span>点击次数：</span>
                                    <span className="countNum">{item.count}</span>  
                                <button onClick={()=>goodClick(item,"good")} className="imgBtn">
                                    <img src={goodImg} className="imgs"></img>
                                    <span className="goodNum">{item.good}</span>
                                </button>
                                <button onClick={()=>goodClick(item,"bad")} className="imgBtn">
                                    <img src={badImg} className="imgs"></img>
                                    <span className="badNum">{item.bad}</span>
                                </button>
                                <span className="timeShow">{item.time}</span>
                            </div>
                            <div className="className">
                                <p>分类：{item.classify}</p>
                            </div>
                        </li >
                    )
                })}
            </ul >

        </div >
    )
}
var mapStateToProps = (state) => {
    return {
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        updataClick: function (number,id,typeStr) {
            dispatch(clickAction(number,id,typeStr))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(List);