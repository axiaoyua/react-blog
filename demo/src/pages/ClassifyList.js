import {List} from './../components/index'
import {useParams} from 'react-router-dom'
import {useEffect} from 'react'
import { connect } from 'react-redux'
import {getClassifyPageList} from './../store/index'
import './css/home.css'
const ClassifyList=(props)=>{
    let {arr,getList}=props

    let {classify}=useParams()

    useEffect(()=>{
        getList(classify)
    },[])

    return(
        <div className='rightWrap'>
            <List dataArr={arr}></List>
        </div>
    )
}
var mapStateToProps=(state)=>{
    return{
        arr:state.classifyPageList
    }
}
var mapDispatchToProps=(dispatch)=>{
    return{
        getList:function(classify){
            dispatch(getClassifyPageList(classify))
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ClassifyList)