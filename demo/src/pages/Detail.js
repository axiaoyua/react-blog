import { useParams } from 'react-router-dom'
import { connect } from 'react-redux'
import { getPageDetail, clickAction, getPageComment, addNewComment } from './../store/index'
import { useEffect, useState } from 'react'
import './css/detail.css'
import './css/index.css'
import goodImg from './../imgs/good.png'
import badImg from './../imgs/bad.png'
const Detail = (props) => {
    let { data, comment, getDetail, updataClick, getComment, addComment } = props

    let { classify, id } = useParams()

    const [value, setValua] = useState('')

    useEffect(() => {
        getDetail(id)
        getComment(id)
    }, [])

    const goodClick = (type) => {
        if (type === "good") {
            data.good++
            updataClick(data.good, data._id, "good")
            let num = document.getElementsByClassName('goodNum')[0]
            num.innerText++
        } else {
            data.bad++
            updataClick(data.bad, data._id, "bad")
            let num = document.getElementsByClassName('badNum')[0]
            num.innerText++
        }
    }
    const addCommentClick = () => {
        if (!value) {
            alert('请输入内容')
        } else {
            addComment(value, id, data.title)
            setValua('')
            let commentInput = document.getElementById('commentInput')
            commentInput.value = ''
            alert('评论提交成功，待审核')
        }
    }
    const inputChange = (e) => {
        setValua(e.target.value)
    }
    return (
        <div className='pageBox'>
            <h1>{data.title}</h1>
            <span className="pageBody">{data.content}</span>
            <div className="foot">
                <span>点击次数：{data.count}</span>
                <button onClick={() => goodClick("good")}>
                    <img src={goodImg} className="imgs"></img>
                    <span className='goodNum'>{data.good}</span>
                </button>
                <button onClick={() => goodClick("bad")}>
                    <img src={badImg} className="imgs"></img>
                    <span className='badNum'>{data.bad}</span>
                </button>
            </div>
            <p>{data.time}</p>
            <div className="classifyFoot">
                分类专栏：
                <button>{data.classify}</button>
            </div>
            <div className='addComment'>
                <h1>添加评论</h1>
                <input onChange={(e) => inputChange(e)} id='commentInput'></input>
                <button onClick={() => addCommentClick()}>提交</button>
            </div>
            <div className='commentShow'>
                <h1>评论展示</h1>
                {comment.length === 0 ? <div>暂时还没有评论哦~</div> :
                    <div>
                        <ul>
                            {comment.map((item, index) => {
                                return <li key={index}>{item.content}</li>
                            })}
                        </ul>
                    </div>
                }
            </div>
        </div>
    )
}
var mapStateToProps = (state) => {
    return {
        data: state.pageDetail,
        comment: state.pageComment
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        getDetail: function (id) {
            dispatch(getPageDetail(id))
        },
        updataClick: function (number, id, typeStr) {
            dispatch(clickAction(number, id, typeStr))
        },
        getComment: function (id) {
            dispatch(getPageComment(id))
        },
        addComment: function (content, id, title) {
            dispatch(addNewComment(content, id, title))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Detail)