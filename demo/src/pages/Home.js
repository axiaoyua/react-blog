import { useNavigate } from 'react-router-dom'
import { connect } from 'react-redux';
import './css/home.css'
import { useEffect } from 'react'

import {getClassifyList,getAllPage} from './../store/index'
import {List} from './../components/index'
const Home = (props) => {
    let { arr,showArr,getList,getAllPageList } = props

    const navigation = useNavigate()

    useEffect(() => {
        getList()
        getAllPageList()
    }, [])

    const goToClassifyList=(item)=>{
        navigation(`/page/${item}`)
    }
    return (
        <div className='rightWrap'>
            <h1>分类</h1>
            <div className='classifyList'>
                <ul>
                    {arr.map((item, index) => {
                        return <li key={index} onClick={()=>goToClassifyList(item)}>{item}</li>
                    })}
                </ul>
            </div>
            <h1>文章展示</h1>
            <List dataArr={showArr}></List>
        </div>
    )
}
var mapStateToProps = (state) => {
    return {
        arr: state.classifyListArr,
        showArr:state.allPagelist
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        getList: function () {
            dispatch(getClassifyList())
        },
        getAllPageList:function(){
            dispatch(getAllPage())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)