import { connect } from 'react-redux'
import {getHotPages} from './../store/index'
import {useEffect} from 'react'
import {List} from './../components/index'
import './css/home.css'
const Hot = (props) => {
    let {arr,getHotPage}=props
    useEffect(()=>{
        getHotPage()
    },[])
    return (
        <div className='rightWrap'>
            <h1>热门推荐</h1>
            <List dataArr={arr}></List>
        </div>
    )
}
var mapStateToProps = (state) => {
    return {
        arr:state.hotList
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        getHotPage:function(){
            dispatch(getHotPages())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Hot)