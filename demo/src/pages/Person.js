import './css/index.css'
import { connect } from 'react-redux'
import { getAllComments, checkComment } from './../store/index'
import { useEffect } from 'react'
const Person = (props) => {
    let { arr, getAll, check } = props

    useEffect(() => {
        getAll()
    }, [])

    const passClick = (item) => {
        check(item._id, "pass")
    }
    const nopassClick = (item) => {
        check(item._id, "nopass")
    }
    const deleteClick = (item) => {
        check(item._id, "delete")
    }

    const state0 = (item) => {
        return (
            <div>
                <button onClick={() => passClick(item)}>通过</button>
                <button onClick={() => nopassClick(item)}>不通过</button>
                <button onClick={() => deleteClick(item)}>删除</button>
            </div>
        )
    }
    const state1 = (item) => {
        return (
            <div>
                <button onClick={() => nopassClick(item)}>不通过</button>
                <button onClick={() => deleteClick(item)}>删除</button>
            </div>
        )
    }
    const state2 = (item) => {
        return (
            <div>
                <button onClick={() => passClick(item)}>通过</button>
                <button onClick={() => deleteClick(item)}>删除</button>
            </div>
        )
    }
    return (
        <div className='rightWrap'>
            <h1>审核评论</h1>
            <div className='personWrap'>
                <ul>
                    {arr.map((item, index) => {
                        return (
                            <li key={index}>
                                <h5>文章标题：{item.articleTitle}</h5>

                                <h5>评论内容：{item.content}</h5>

                                <div className='personBtnWrap'>
                                    {
                                        item.state === 0 ?
                                            state0(item)
                                            : item.state === 1 ?
                                                state1(item)
                                                :
                                                state2(item)
                                    }
                                </div>


                            </li>

                        )
                    })}
                </ul>
            </div>
        </div>
    )
}
var mapStateToProps = (state) => {
    return {
        arr: state.allComment
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        getAll: function () {
            dispatch(getAllComments())
        },
        check: function (id, type,) {
            dispatch(checkComment(id, type))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Person);