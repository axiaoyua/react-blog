import './css/index.css'
import { useState } from 'react'
import { connect } from 'react-redux'
import { addNewPage } from './../store/index'
const Write = (props) => {
    let { add } = props

    const [titleValue, setTitleValue] = useState('')
    const [contentValue, setcontentValue] = useState('')
    const [classifyValue, setClassifyValue] = useState('')

    const inputChange1 = (e) => {
        setTitleValue(e.target.value)
    }
    const inputChange2 = (e) => {
        setcontentValue(e.target.value)
    }
    const inputChange3 = (e) => {
        setClassifyValue(e.target.value)
    }

    const addPage = () => {
        if(!titleValue){
            alert('标题不能为空')
        }
        if(!contentValue){
            alert('内容不能为空')
        }
        if(!classifyValue){
            alert('分类不能为空')
        }
        add(titleValue, contentValue, classifyValue)
        setTitleValue("")
        setcontentValue("")
        setClassifyValue("")
        let titleInput = document.getElementsByClassName('titleInput')[0]
        let contentInput = document.getElementsByClassName('contentInput')[0]
        let classifyInput = document.getElementsByClassName('classifyInput')[0]
        titleInput.value = ''
        contentInput.value = ''
        classifyInput.value = ''
        alert('发表文章成功')
    }
    return (
        <div className='rightWrap'>
            <h1>发表文章</h1>
            <div className='inputWrap'>
                <input placeholder='请输入标题' className='titleInput' onChange={(e) => inputChange1(e)}></input>
                <textarea placeholder='请输入内容' className='contentInput' onChange={(e) => inputChange2(e)}></textarea>
                <input placeholder='请输入分类' className='classifyInput' onChange={(e) => inputChange3(e)}></input>
                <button onClick={() => addPage()}>提交</button>
            </div>
        </div>
    )
}
var mapStateToProps = (state) => {
    return {
    }
}
var mapDispatchToProps = (dispatch) => {
    return {
        add: function (title, content, classify) {
            dispatch(addNewPage(title, content, classify))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Write);