import Home from './Home'
import Hot from './Hot'
import Write from './Write'
import Person from './Person'
import Detail from './Detail'
import ClassifyList from './ClassifyList'

export {
    Home,
    Hot,
    Write,
    Person,
    Detail,
    ClassifyList
}