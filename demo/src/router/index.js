import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import {
    Home,
    Hot,
    Write,
    Person,
    Detail,
    ClassifyList
} from './../pages/index'
import { LeftNav } from './../components/index'
const AppRouter = () => (
    <Router>
        <Routes>
            <Route path='/' element={<LeftNav />}>
                <Route path='/home' element={<Home />}></Route>
                <Route path='/hot' element={<Hot />}></Route>
                <Route path='/write' element={<Write />}></Route>
                <Route path='/person' element={<Person />}></Route>
                <Route path='/page/:classify' exact element={<ClassifyList />}></Route>
                <Route path='/page/:classify/:id' exact element={<Detail />}></Route>
            </Route>
        </Routes>
    </Router>

)
export default AppRouter