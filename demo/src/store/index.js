import { createStore, compose, applyMiddleware } from 'redux';
import axios from 'axios'

import thunkMiddleware from 'redux-thunk'
const middlewares = [thunkMiddleware];
const storeEnhanres = compose(applyMiddleware(...middlewares), (window && window.devToolsExtension) ? window.devToolsExtension() : (f) => f)

let initState = {
    classifyListArr: [],  //home页的分类栏
    allPagelist: [],  //home页的所有文章展示
    classifyPageList: [],     //分类文章列表
    pageDetail: {},    // 文章详情页数据
    hotList: [],   // 热门文章列表
    pageComment: [], // 文章详情页的评论
    allComment: []  // 所有评论
    // allComment在reducer里改变了，在组件里也是会动态改变的
}
let reducer = (state, action) => {
    switch (action.type) {
        case "GetClassifyListSUCCESS": {
            return {
                ...state,
                classifyListArr: action.list
            }
        }
        case "GetAllPagesSuccess": {
            return {
                ...state,
                allPagelist: action.list
            }
        }
        case "UpdataClickSuccess": {
            return {
                state
            }
        }
        case "GetClassifyPageListSUCCESS": {
            return {
                ...state,
                classifyPageList: action.list
            }
        }
        case "GetPageDetailSuccess": {
            return {
                ...state,
                pageDetail: action.obj
            }
        }
        case "GetHotPageSuccess": {
            return {
                ...state,
                hotList: action.list
            }
        }
        case "AddNewPageSuccess": {
            return state
        }
        case "GetPageCommentSuccess": {
            return {
                ...state,
                pageComment: action.list
            }
        }
        case "AddNewCommentSuccess": {
            return state
        }
        case "GetAllCommentsSuccess": {
            return {
                ...state,
                allComment: action.list
            }
        }
        case "CheckCommentSuccess": {
            let newArr = state.allComment.concat([])
            if (action.typeStr === "pass") {
                for (let i = 0; i < newArr.length; i++) {
                    if (newArr[i]._id === action.id) {
                        newArr[i].state = 1
                    }
                }
            }else if(action.typeStr === "nopass"){
                for (let i = 0; i < newArr.length; i++) {
                    if (newArr[i]._id === action.id) {
                        newArr[i].state = 2
                    }
                }
            }else{
                for (let i = 0; i < newArr.length; i++) {
                    if (newArr[i]._id === action.id) {
                        newArr.splice(i,1)
                    }
                }
            }
            state.allComment = newArr
            return {
                ...state
            }
        }
        case "LOADING": {
            return state
        }
        case "FAIL": {
            return state
        }
        default: return state
    }
}

// 获取分类列表
function getClassifyList() {
    return function (dispatch) {
        dispatch(axiosLoading())                // 加载中
        axios.post('http://localhost:9999/allclassify').then(res => {
            dispatch(getClassifyListSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}


// 获取所有文章
function getAllPage() {
    return function (dispatch) {
        dispatch(axiosLoading())                // 加载中
        axios.post('http://localhost:9999/').then(res => {
            // console.log(res.data);
            dispatch(getAllPagesSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 更改count/good/bad。axios传参：number,id,typeStr
function clickAction(number, id, typeStr) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/updataclick', { number, id, typeStr }).then(res => {
            dispatch(updataClickSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 获取文章分类列表。axios参数：classify
function getClassifyPageList(classify) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/classifyList', { classify }).then(res => {
            dispatch(getClassifyPageListSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 获取文章详情页数据。axios参数：_id
function getPageDetail(id) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/detail/page', { id }).then(res => {
            dispatch(getPageDetailSuccess(res.data[0]))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 获取热门文章
function getHotPages() {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/allSortBlogs').then(res => {
            dispatch(getHotPageSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 发表文章
function addNewPage(title, content, classify) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/addNewBlogs', { title, content, classify }).then(res => {
            dispatch(addNewPageSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}


// 发表评论。（要把文章id一起提交上来哦）
function addNewComment(content, id, title) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/manage/addComment', { content, id, title }).then(res => {
            dispatch(addNewCommentSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 获取文章详情页评论。axios参数：文章id
function getPageComment(id) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/manage/getComment', { id }).then(res => {
            dispatch(getPageCommentSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// 获取所有评论
function getAllComments() {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/manage/getAllComments').then(res => {
            dispatch(getAllCommentsSuccess(res.data))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}


// 审核评论。axios参数：评论id，类型  id,type (pass,nopass,delete)
function checkComment(id, type) {
    return function (dispatch) {
        dispatch(axiosLoading())
        axios.post('http://localhost:9999/manage/checkComment', { id, type }).then(res => {
            dispatch(checkCommentSuccess({ id, typeStr: type }))   // 加载成功
        }).catch(err => {
            dispatch(axiosFail())              // 加载中失败
        })
    }
}

// axiosLoading和axiosFail函数可以复用，但是Success函数每次都要重新写
function axiosLoading() {
    return {
        type: "LOADING"
    }
}
function getClassifyListSuccess(e) {
    return {
        type: "GetClassifyListSUCCESS",
        list: e
    }
}
function getAllPagesSuccess(e) {
    return {
        type: "GetAllPagesSuccess",
        list: e
    }
}
function updataClickSuccess(e) {
    return {
        type: "UpdataClickSuccess"
    }
}
function getClassifyPageListSuccess(e) {
    return {
        type: "GetClassifyPageListSUCCESS",
        list: e
    }
}
function getPageDetailSuccess(e) {
    return {
        type: "GetPageDetailSuccess",
        obj: e
    }
}
function getHotPageSuccess(e) {
    return {
        type: "GetHotPageSuccess",
        list: e
    }
}
function addNewPageSuccess() {
    return {
        type: "AddNewPageSuccess"
    }
}
function getPageCommentSuccess(e) {
    return {
        type: "GetPageCommentSuccess",
        list: e
    }
}
function addNewCommentSuccess() {
    return {
        type: "AddNewCommentSuccess"
    }
}
function getAllCommentsSuccess(e) {
    return {
        type: "GetAllCommentsSuccess",
        list: e
    }
}
function checkCommentSuccess(e) {
    return {
        type: "CheckCommentSuccess",
        id: e.id,
        typeStr: e.typeStr
    }
}

function axiosFail() {
    return {
        type: "FAIL"
    }
}


// 导出action函数
export {
    getClassifyList,
    getAllPage,
    clickAction,
    getClassifyPageList,
    getPageDetail,
    getHotPages,
    addNewPage,
    getPageComment,
    addNewComment,
    getAllComments,
    checkComment
}



const store = createStore(reducer, initState, storeEnhanres)

export default store