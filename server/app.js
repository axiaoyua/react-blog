const express =require('express')

const app =express()

// 引入cors，解决跨域
const cors=require('cors')
app.use(cors())

// 解析post请求
app.use(express.urlencoded({extended:false}))
app.use(express.json())

// 分摊路由
app.use('/',require('./router/home'))
app.use('/detail',require('./router/detail'))
app.use('/manage',require('./router/manage'))

app.listen(9999)