const mongoose = require('./db');
// 表：评论

const Comments=mongoose.Schema({
    content:String,
    time:String,
    articleTitle:String,
    articleId:String,
    state:Number
})

const CommentsModel=mongoose.model('comments',Comments)
const commentTest1=new CommentsModel({
    content:'JS1的评论：Lorem ipsum, aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    articleTitle:'JS1',
    articleId:'625c1870a8a8cf4ee2e6f1a9',
    state:1
})
const commentTest2=new CommentsModel({
    content:'JS1的评论：Lorem ipsum,  aliquam impedit? aliquam impedit?aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    articleTitle:'JS1',
    articleId:'625c1870a8a8cf4ee2e6f1a9',
    state:1
})
const commentTest3=new CommentsModel({
    content:'JS2的评论：Lorem ipsum, aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    articleTitle:'JS2',
    articleId:'625c1870a8a8cf4ee2e6f1aa',
    state:1
})
const commentTest4=new CommentsModel({
    content:'JS2的评论：Lorem ipsum,  aliquam impedit? aliquam impedit?aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    articleTitle:'JS2',
    articleId:'625c1870a8a8cf4ee2e6f1aa',
    state:1
})
// commentTest1.save()
// commentTest2.save()
// commentTest3.save()
// commentTest4.save()
module.exports = CommentsModel