const mongoose = require('./db');
// 表：文章

// 文章的属性 
const Pages=mongoose.Schema({
    title : String,
    content:String,
    time : String,
    count : Number,
    good:Number,
    bad:Number,
    classify : String
})  
const PagesModel=mongoose.model('pages',Pages)


const pageTest1=new PagesModel({
    title:'JS1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'js'
})

const pageTest2=new PagesModel({
    title:'JS2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'js'
})
const pageTest3=new PagesModel({
    title:'node1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'node'
})
const pageTest4=new PagesModel({
    title:'node2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'node'
})
const pageTest5=new PagesModel({
    title:'http1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'http'
})
const pageTest6=new PagesModel({
    title:'http2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'http'
})
const pageTest7=new PagesModel({
    title:'ajax1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'ajax'
})
const pageTest8=new PagesModel({
    title:'ajax2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'ajax'
})
const pageTest9=new PagesModel({
    title:'react1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'react'
})
const pageTest10=new PagesModel({
    title:'react2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'react'
})
const pageTest11=new PagesModel({
    title:'VUE1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'vue'
})

const pageTest12=new PagesModel({
    title:'VUE2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'vue'
})
const pageTest13=new PagesModel({
    title:'RN1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'rn'
})
const pageTest14=new PagesModel({
    title:'RN2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'rn'
})
const pageTest15=new PagesModel({
    title:'java1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'java'
})
const pageTest16=new PagesModel({
    title:'java2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'java'
})
const pageTest17=new PagesModel({
    title:'mongo1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'mongodb'
})
const pageTest18=new PagesModel({
    title:'mongo2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'mongodb'
})

// pageTest1.save()
// pageTest2.save()
// pageTest3.save()
// pageTest4.save()
// pageTest5.save()
// pageTest6.save()
// pageTest7.save()
// pageTest8.save()
// pageTest9.save()
// pageTest10.save()
// pageTest11.save()
// pageTest12.save()
// pageTest13.save()
// pageTest14.save()
// pageTest15.save()
// pageTest16.save()
// pageTest17.save()
// pageTest18.save()



module.exports = PagesModel

// 初始化数据：cd server  cd models  node pages.js