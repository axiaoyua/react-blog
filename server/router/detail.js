const express = require('express')

const router = express.Router()

const PagesModel=require('../models/pages')

// 文章详情页
router.post('/page',(req,res)=>{
    let {id}=req.body
    PagesModel.find({_id:id}).then(data=>{
        res.send(data)
    })
})
module.exports = router