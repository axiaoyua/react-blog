const express = require('express')

const router = express.Router()

const moment = require('moment')

const PagesModel=require('../models/pages')

// 主页，分类列表页
router.post('/',(req,res)=>{
    PagesModel.find().then(data=>{
        // console.log(data);
        res.send(data)
    })
})

router.post('/allclassify',(req,res)=>{
    PagesModel.find().then(data=>{
        let classifyArr=[]
        for(let i=0;i<data.length;i++){
            classifyArr.push(data[i].classify)
        }
        classifyArr=Array.from(new Set(classifyArr))
        res.send(classifyArr)
    })
})
router.post('/classifyList',(req,res)=>{
    let classify=req.body
    PagesModel.find(classify).then(data=>{
        res.send(data)
    })
})
router.post('/updataclick',(req,res)=>{
    let {number,id,typeStr}=req.body
    if(typeStr=='count'){
        PagesModel.updateOne({_id:id},{count:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }else if(typeStr=='good'){
        PagesModel.updateOne({_id:id},{good:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }else{
        PagesModel.updateOne({_id:id},{bad:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }
    
    
})

router.post('/allSortBlogs',(req,res)=>{
    PagesModel.find({}).sort({count:-1}).then(data=>{
        res.send(data)
    })
})

router.post('/addNewBlogs',(req,res)=>{
    let {title,content,classify}=req.body
    classify=classify.toLowerCase()
    const newPage=new PagesModel({
        title,
        content,
        classify,
        count:0,
        good:0,
        bad:0,
        time:moment().format('MMMM Do YYYY, h:mm:ss a')
    })
    newPage.save().then(data=>{
        res.send(data)
    })
})
module.exports = router